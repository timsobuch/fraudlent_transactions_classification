import tensorflow as tf
import numpy as np
from prep_data import DataPreparation as DP
import keras.optimizers
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Activation, Convolution1D, LSTM, Flatten, Dropout, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import Callback
import keras.backend as K
from keras.layers.advanced_activations import LeakyReLU


class NeuralNet:
    def __init__(self, hidden_nodes, training_epochs, training_dropout,
                 batch_size, learning_rate, filename, drop_features, drop_params_bool):
        self.hidden_nodes = hidden_nodes
        self.training_epochs = training_epochs
        self.training_dropout = training_dropout
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.filename = filename
        self.drop_features = drop_features
        self.drop_params_bool = drop_params_bool

        self.input_nodes = 0
        self.acc_summary = []
        self.loss_summary = []
        self.val_accuracy_summary = []
        self.val_loss_summary = []
        self.metrics_summary = []

    def net_arch(self):
        #TODO: оптимизировать архитектуру сетки для использования сверточных и LSTM-слоев
        model = Sequential()

        # model.add(Dense(self.hidden_nodes, input_dim=self.input_nodes, activation='relu'))
        # model.add(Convolution1D(filters=12, kernel_size=2, strides=1, activation='relu'))
        # model.add(Convolution1D(filters=12, kernel_size=3, strides=1, activation='relu'))
        # model.add(Convolution1D(filters=12, kernel_size=4, strides=1, activation='relu'))
        model.add(Dense(self.hidden_nodes, input_dim=self.input_nodes, activation='relu', kernel_regularizer=l2(0.01)))
        model.add(Dropout(self.training_dropout))
        # model.add(LSTM(self.hidden_nodes, return_sequences=True,
        #                  activation='relu'))
        model.add(Dense(self.hidden_nodes, activation='relu', kernel_regularizer=l2(0.01)))
        model.add(Dropout(self.training_dropout))
        model.add(Dense(self.hidden_nodes, activation='relu', kernel_regularizer=l2(0.01)))
        model.add(Dropout(self.training_dropout))
        model.add(Dense(self.hidden_nodes, activation='relu', kernel_regularizer=l2(0.01)))
        model.add(Dropout(self.training_dropout))
        model.add(Dense(2, activation='softmax'))

        return model


    def learn_nn(self):
        np.random.seed(5)
        data_loader = DP(self.filename, self.drop_features, self.drop_params_bool)
        input_X, input_Y, input_X_test, input_Y_test = data_loader.split_learning_dataset()
        self.input_nodes = data_loader.get_number_of_columns()

        nnet = self.net_arch()
        nnet.compile(optimizer=keras.optimizers.Adam(lr=self.learning_rate, beta_1=0.9, beta_2=0.999,
                                                     epsilon=None, decay=0.0, amsgrad=False),
                          loss='mean_squared_error',
                          # metrics=['accuracy'])
                          metrics=['mae', 'acc'])

        lhistory = nnet.fit(input_X, input_Y, epochs=self.training_epochs, batch_size=self.batch_size,
                 validation_split=0.1)
        self.acc_summary = lhistory.history['acc']
        self.loss_summary = lhistory.history['loss']
        self.val_accuracy_summary = lhistory.history['val_acc']
        self.val_loss_summary = lhistory.history['val_loss']
        self.metrics_summary = lhistory.history['mean_absolute_error']

    def get_data_to_plot(self):
        return self.acc_summary, self.loss_summary, self.val_accuracy_summary, self.val_loss_summary, self.metrics_summary


if __name__ == '__main__':
    # DEBUG
    NN = NeuralNet(hidden_nodes=64, training_epochs=2000,
                   training_dropout=0.9, batch_size=2048, learning_rate=0.005, filename = 'data/creditcard.csv',
                   drop_features = ['V28', 'V27', 'V26', 'V25', 'V24', 'V23', 'V22', 'V20', 'V15', 'V13', 'V8'],
                   drop_params_bool=True)
    NN.learn_nn()
