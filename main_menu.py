import tkinter.messagebox
import sys
from prep_data import DataPreparation
from plot import SetOfPlotters
from neuralnet import NeuralNet
from optparse import OptionParser



class MainMenu:
    def __init__(self):
        self.filename = 'data/creditcard.csv'
        self.drop_features = ['V28', 'V27', 'V26', 'V25', 'V24', 'V23', 'V22', 'V20', 'V15', 'V13', 'V8']
        self.drop_params_bool = True

    def plot_distributions(self):
        DP = DataPreparation(self.filename,
                             self.drop_features, self.drop_params_bool)
        plotter_toolbox = SetOfPlotters()
        plotter_toolbox.plot_distrubutions_for_features(DP.get_data_frame())


    def teach_nn(self):
        NN = NeuralNet(hidden_nodes=64, training_epochs=2500,
                       training_dropout=0.9, batch_size=1024, learning_rate=0.009, filename=self.filename, drop_features=self.drop_features,
                       drop_params_bool=self.drop_params_bool)
        NN.learn_nn()
        plotter_toolbox = SetOfPlotters()
        acc_summary, loss_summary, val_accuracy_summary, val_loss_summary, metrics_summary = NN.get_data_to_plot()
        plotter_toolbox.plot_the_nn_learning_values(acc_summary, loss_summary, val_accuracy_summary, val_loss_summary,
                                                    metrics_summary)

    def on_closure(self):
        sys.exit()

if __name__ == "__main__":
    parser = OptionParser()
    main_menu = MainMenu()

    parser.add_option("-f", "--function", help="Функция для запуска: histograms или teach_nn", default='histograms')
    (options, args) = parser.parse_args()

    if not options.function:
        parser.error('Ошибка. Выберете функцию для исполнения')
    config_function = options.function
    if config_function == 'histograms':
        main_menu.plot_distributions()
    elif config_function == 'teach_nn':
        main_menu.teach_nn()
    else:
        print("Ошибка. Неверная функция для исполнения.")
