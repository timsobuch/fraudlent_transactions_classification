import numpy as np
import pandas as pd
import re
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.utils import shuffle


class DataPreparation:
    def __init__(self, csv_data, drop_list, drop_features_bool):
        self.dataframe = pd.read_csv(csv_data)
        self.drop_list = drop_list
        self.drop_features_bool = drop_features_bool

        self.data_shape = 0
        self.number_of_columns = 0

    def get_data_frame(self):
        return self.dataframe

    def get_drop_list(self):
        return self.drop_list

    def get_data_shape(self):
        return self.data_shape

    def get_number_of_columns(self):
        return self.number_of_columns

    def get_number_of_rows(self):
        return len(self.dataframe)

    def drop_useless_data(self):
        self.dataframe = self.dataframe.drop(self.drop_list, axis=1)

    def create_features(self):
        # TODO: автоматизировать создание фич
        ### FROM: https://www.kaggle.com/currie32/predicting-fraud-with-tensorflow
        self.dataframe['V1_'] = self.dataframe.V1.map(lambda x: 1 if x < -3 else 0)
        self.dataframe['V2_'] = self.dataframe.V2.map(lambda x: 1 if x > 2.5 else 0)
        self.dataframe['V3_'] = self.dataframe.V3.map(lambda x: 1 if x < -4 else 0)
        self.dataframe['V4_'] = self.dataframe.V4.map(lambda x: 1 if x > 2.5 else 0)
        self.dataframe['V5_'] = self.dataframe.V5.map(lambda x: 1 if x < -4.5 else 0)
        self.dataframe['V6_'] = self.dataframe.V6.map(lambda x: 1 if x < -2.5 else 0)
        self.dataframe['V7_'] = self.dataframe.V7.map(lambda x: 1 if x < -3 else 0)
        self.dataframe['V9_'] = self.dataframe.V9.map(lambda x: 1 if x < -2 else 0)
        self.dataframe['V10_'] = self.dataframe.V10.map(lambda x: 1 if x < -2.5 else 0)
        self.dataframe['V11_'] = self.dataframe.V11.map(lambda x: 1 if x > 2 else 0)
        self.dataframe['V12_'] = self.dataframe.V12.map(lambda x: 1 if x < -2 else 0)
        self.dataframe['V14_'] = self.dataframe.V14.map(lambda x: 1 if x < -2.5 else 0)
        self.dataframe['V16_'] = self.dataframe.V16.map(lambda x: 1 if x < -2 else 0)
        self.dataframe['V17_'] = self.dataframe.V17.map(lambda x: 1 if x < -2 else 0)
        self.dataframe['V18_'] = self.dataframe.V18.map(lambda x: 1 if x < -2 else 0)
        self.dataframe['V19_'] = self.dataframe.V19.map(lambda x: 1 if x > 1.5 else 0)
        self.dataframe['V21_'] = self.dataframe.V21.map(lambda x: 1 if x > 0.6 else 0)

    def prep_learning_dataset(self):
        if self.drop_features_bool:
            self.drop_useless_data()
        self.number_of_columns = len(self.dataframe.columns)-1

        self.dataframe.loc[self.dataframe.Class == 0, 'Normal'] = 1
        self.dataframe.loc[self.dataframe.Class == 1, 'Normal'] = 0

        self.dataframe = self.dataframe.rename(columns={'Class': 'Fraud'})
        Fraud = self.dataframe[self.dataframe.Fraud == 1]
        Normal = self.dataframe[self.dataframe.Normal == 1]
        X_train = Fraud.sample(frac=0.8)
        count_Frauds = len(X_train)

        X_train = pd.concat([X_train, Normal.sample(frac=0.8)], axis=0)

        X_test = self.dataframe.loc[~self.dataframe.index.isin(X_train.index)]
        X_train = shuffle(X_train)
        X_test = shuffle(X_test)

        y_train = X_train.Fraud
        y_train = pd.concat([y_train, X_train.Normal], axis=1)

        y_test = X_test.Fraud
        y_test = pd.concat([y_test, X_test.Normal], axis=1)

        X_train = X_train.drop(['Fraud', 'Normal'], axis=1)
        X_test = X_test.drop(['Fraud', 'Normal'], axis=1)

        ratio = len(X_train) / count_Frauds

        y_train.Fraud *= ratio
        y_test.Fraud *= ratio

        features = X_train.columns.values

        for feature in features:
            mean, std = self.dataframe[feature].mean(), self.dataframe[feature].std()
            X_train.loc[:, feature] = (X_train[feature] - mean) / std
            X_test.loc[:, feature] = (X_test[feature] - mean) / std
        ### END FROM
        return X_train, y_train, X_test, y_test


    def split_learning_dataset(self):
        X_train, y_train, X_test, y_test = self.prep_learning_dataset()
        split = int(len(y_test) / 2)

        input_X = X_train
        input_Y = y_train
        input_X_test = X_test[split:]
        input_Y_test = y_test[split:]

        return input_X, input_Y, input_X_test, input_Y_test



if __name__ == '__main__':
    # DEBUG
    DP = DataPreparation('data/creditcard.csv', ['V28', 'V27', 'V26', 'V25', 'V24', 'V23', 'V22', 'V20', 'V15', 'V13',
                                                 'V8'], True)
    x = DP.split_learning_dataset()