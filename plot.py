import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.gridspec as gridspec
from neuralnet import NeuralNet


class SetOfPlotters:
    def __init__(self):
        sns.set_style("whitegrid")

    def plot_distrubutions_for_features(self, dataframe):
        features = dataframe.ix[:,1:29].columns
        plt.figure(figsize=(10,10))
        grid_spec = gridspec.GridSpec(7, 4)
        for i, param in enumerate(dataframe[features]):
            ax = plt.subplot(grid_spec[i])
            sns.distplot(dataframe[param][dataframe.Class == 1], bins=100)
            sns.distplot(dataframe[param][dataframe.Class == 0], bins=100)
            ax.set_xlabel('Гистограмма параметра: ' + str(param))
        plt.tight_layout()
            # ax.set_('Гистограмма параметра: ' + str(param))
            # if i == 30:
        plt.show()

    def plot_the_nn_learning_values(self, acc_summary, loss_summary, val_accuracy_summary, val_loss_summary, metrics_summary):
        field, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex='all', figsize=(15, 6))

        sns.tsplot(acc_summary, ax=ax1, color=sns.xkcd_rgb["dusty purple"])
        sns.tsplot(val_accuracy_summary, ax=ax1, color=sns.xkcd_rgb["amber"])
        ax1.set_title('Acc')

        sns.tsplot(loss_summary, ax=ax2, color=sns.xkcd_rgb["medium green"])
        sns.tsplot(val_loss_summary, ax=ax2, color=sns.xkcd_rgb["windows blue"])
        ax2.set_title('Loss')

        sns.tsplot(metrics_summary, ax=ax3, color=sns.xkcd_rgb["pale red"])
        ax3.set_title('MAE')

        plt.xlabel('Epochs')
        plt.show()


if __name__ == '__main__':
    # DEBUG
    NN = NeuralNet(hidden_nodes=64, training_epochs=2500,
                   training_dropout=0.9, batch_size=2048, learning_rate=0.005)
    plotter_toolbox = SetOfPlotters()
    NN.learn_nn()
    acc_summary, loss_summary, val_accuracy_summary, val_loss_summary, metrics_summary = NN.get_data_to_plot()
    plotter_toolbox.plot_the_nn_learning_values(acc_summary, loss_summary, val_accuracy_summary, val_loss_summary, metrics_summary)